﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.Models
{
    public class Studentas
    {
        public int? Id { get; set; }

        public string Vardas { get; set; }

        public string Pavardė { get; set; }

        public string TelNr { get; set; }

        public string Adresas { get; set; }
    }
}
