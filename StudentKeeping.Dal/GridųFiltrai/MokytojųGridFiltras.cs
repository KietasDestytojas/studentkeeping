﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.GridųFiltrai
{
    public class MokytojųGridFiltras
    {
        public string Vardas { get; set; }

        public string Pavardė { get; set; }

        public List<int> MokytojųIds { get; set; }

        public MokytojųGridFiltras()
        {
            MokytojųIds = new List<int>();
        }
    }
}
