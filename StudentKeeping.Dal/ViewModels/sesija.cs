﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.ViewModels
{
    public class Sesija
    {
        public int NaudotojoId { get; set; }

        public string NaudotojoVardas { get; set; }

        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public Role Role { get; set; }
    }
}
