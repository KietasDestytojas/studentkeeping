﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.ViewModels
{
    public class PasirinktasStudentasUžduočiai
    {
        public int? NaudotojoId { get; set; }

        public string VardasPavardė { get; set; }

        public string Klasė { get; set; }
    }
}
