﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.ViewModels
{
    public enum Role
    {
        Mokytojas = 1,
        Moksleivis = 2
    }
}
