CREATE DATABASE  IF NOT EXISTS `studentkeeping` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `studentkeeping`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: studentkeeping
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `studentai`
--

DROP TABLE IF EXISTS `studentai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentai` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vardas` varchar(100) DEFAULT NULL,
  `Pavarde` varchar(200) DEFAULT NULL,
  `TelNr` varchar(20) DEFAULT NULL,
  `Adresas` varchar(200) DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentai`
--

LOCK TABLES `studentai` WRITE;
/*!40000 ALTER TABLE `studentai` DISABLE KEYS */;
INSERT INTO `studentai` VALUES (1,'Arūnas','Pavarde','','Aguonu g. 1',1),(2,'Anastasija','Pavarde','869545159','',1),(3,'Aronas','Pavarde3','869545159','Geliu g. 23',1),(4,'Saulius','Pavarde1',NULL,'Aguonu g. 1',1),(5,'Mantas','Pavarde2','869545159',NULL,1),(6,'Tadas','Pavarde3','869545159','Geliu g. 23',1),(7,'Tadeuš','Pavarde1',NULL,'Aguonu g. 1',1),(8,'Martynas','Pavarde2','869545159',NULL,0),(9,'Stanislavas','Pavarde3','869545159','Geliu g. 23',1),(10,'Viktorija','Pavarde1',NULL,'Aguonu g. 1',1),(11,'Aušra','Pavarde2','869545159',NULL,0),(12,'Saulė','Pavarde3','869545159','Geliu g. 23',1);
/*!40000 ALTER TABLE `studentai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'studentkeeping'
--

--
-- Dumping routines for database 'studentkeeping'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-13 10:51:05
