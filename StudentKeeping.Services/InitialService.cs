﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentKeeping.Dal;
using MySql.Data.MySqlClient;

namespace StudentKeeping.Services
{
    public static class InitialService
    {
        public static void InitialDatabase()
        {
            try
            {
                var dBConnection = new DBConnection("Sys");
                dBConnection.Connection.Open();
                var createDbSql = "create database if not exists StudentKeeping;";
                var createDbCommand = new MySqlCommand(createDbSql, dBConnection.Connection);

                var result = createDbCommand.ExecuteNonQuery();
                dBConnection.Connection.Close();

                dBConnection.ChangeConnection("StudentKeepingConnectionString");

                var createStudentsTableSql = @"create table if not exists Studentai (
	                                                Id int auto_increment,
                                                    Vardas varchar(100),
                                                    Pavarde varchar(200),
                                                    TelNr varchar(20),
                                                    Adresas varchar(200),
                                                    primary key(Id)
                                                );";

                var createStudentsTableCommand = new MySqlCommand(createStudentsTableSql, dBConnection.Connection);
                dBConnection.Connection.Open();
                result = createStudentsTableCommand.ExecuteNonQuery();

                //var addStudentsSql = @"
                //        INSERT INTO Studentai (Vardas, Pavarde, TelNr, Adresas) 
                //        VALUES 
                //        ('Stundetas1', 'Pavarde1', NULL, 'Aguonu g. 1'),
                //        ('Stundetas2', 'Pavarde2', '869545159', NULL),
                //        ('Stundetas3', 'Pavarde3', '869545159', 'Geliu g. 23');";

                //var addStudentsCommand = new MySqlCommand(addStudentsSql, dBConnection.Connection);
                //addStudentsCommand.ExecuteNonQuery();

                dBConnection.Connection.Close();
            }
            catch (Exception ex)
            {
            }

        }

    }
}
