﻿using StudentKeeping.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentKeeping.Dal.Models;
using StudentKeeping.Dal.GridųFiltrai;
using MySql.Data.MySqlClient;
using System.Data;

namespace StudentKeeping.Services
{
    public class MokytojaiService : DBConnection
    {
        public MokytojaiService() { }

        public DataTable GaukMokytojus(MokytojųGridFiltras mokytojųGridFiltras)
        {
            var gaukMokytojusCmdText = @"SELECT Mokytojai.Id
                                                ,Vardas
                                                ,Pavarde
                                                ,DalykoId
                                                ,Pavadinimas
                                         FROM Mokytojai INNER JOIN Dalykai ON Mokytojai.DalykoId = Dalykai.Id
                                         WHERE
                                         (@Dalykai IS NULL OR DalykoId in (@Dalykai)) AND
                                         (@Vardas IS NULL OR Vardas LIKE CONCAT('%', @Vardas, '%')) AND
                                         (@Pavardė IS NULL OR Pavarde LIKE CONCAT('%', @Pavardė, '%'))";

            var gaukMokytojusCmd = new MySqlCommand(gaukMokytojusCmdText, Connection);
            gaukMokytojusCmd.Parameters.AddWithValue("@Vardas", !string.IsNullOrWhiteSpace(mokytojųGridFiltras.Vardas) ? mokytojųGridFiltras.Vardas : null);
            gaukMokytojusCmd.Parameters.AddWithValue("@Pavardė", !string.IsNullOrWhiteSpace(mokytojųGridFiltras.Pavardė) ? mokytojųGridFiltras.Pavardė : null);
            gaukMokytojusCmd.Parameters.AddWithValue("@Dalykai", mokytojųGridFiltras.MokytojųIds.Count == 0 ? null : mokytojųGridFiltras.MokytojųIds);

            Connection.Open();
            MySqlDataAdapter gaukMokytojusAdapter = new MySqlDataAdapter(gaukMokytojusCmd);
            DataTable mokytojųDataTable = new DataTable();
            gaukMokytojusAdapter.Fill(mokytojųDataTable);
            Connection.Close();

            return mokytojųDataTable;
        }
    }
}
