﻿namespace StudentKeeping.Presentation.Mokytojai
{
    partial class MokytojųSąrašas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pavardė = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dalykas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DalykoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxDalykai = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnIeškoti = new System.Windows.Forms.Button();
            this.txtPavardė = new System.Windows.Forms.TextBox();
            this.txtVardas = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Vardas,
            this.Pavardė,
            this.Dalykas,
            this.DalykoId});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 140);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(346, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // Vardas
            // 
            this.Vardas.HeaderText = "Vardas";
            this.Vardas.Name = "Vardas";
            this.Vardas.ReadOnly = true;
            this.Vardas.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Vardas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Pavardė
            // 
            this.Pavardė.HeaderText = "Pavardė";
            this.Pavardė.Name = "Pavardė";
            this.Pavardė.ReadOnly = true;
            this.Pavardė.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Pavardė.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Dalykas
            // 
            this.Dalykas.HeaderText = "Dalykas";
            this.Dalykas.Name = "Dalykas";
            this.Dalykas.ReadOnly = true;
            // 
            // DalykoId
            // 
            this.DalykoId.HeaderText = "DalykoId";
            this.DalykoId.Name = "DalykoId";
            this.DalykoId.ReadOnly = true;
            this.DalykoId.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.listBoxDalykai);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnIeškoti);
            this.groupBox1.Controls.Add(this.txtPavardė);
            this.groupBox1.Controls.Add(this.txtVardas);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 134);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Dalykai:";
            // 
            // listBoxDalykai
            // 
            this.listBoxDalykai.FormattingEnabled = true;
            this.listBoxDalykai.Location = new System.Drawing.Point(78, 61);
            this.listBoxDalykai.Name = "listBoxDalykai";
            this.listBoxDalykai.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxDalykai.Size = new System.Drawing.Size(129, 69);
            this.listBoxDalykai.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vardas:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Pavardė:";
            // 
            // btnIeškoti
            // 
            this.btnIeškoti.Location = new System.Drawing.Point(213, 107);
            this.btnIeškoti.Name = "btnIeškoti";
            this.btnIeškoti.Size = new System.Drawing.Size(53, 23);
            this.btnIeškoti.TabIndex = 2;
            this.btnIeškoti.Text = "Ieškoti";
            this.btnIeškoti.UseVisualStyleBackColor = true;
            this.btnIeškoti.Click += new System.EventHandler(this.btnIeškoti_Click);
            // 
            // txtPavardė
            // 
            this.txtPavardė.Location = new System.Drawing.Point(78, 40);
            this.txtPavardė.Name = "txtPavardė";
            this.txtPavardė.Size = new System.Drawing.Size(129, 20);
            this.txtPavardė.TabIndex = 1;
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(78, 19);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(129, 20);
            this.txtVardas.TabIndex = 0;
            // 
            // MokytojųSąrašas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 290);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "MokytojųSąrašas";
            this.Text = "MokytojųSąrašas";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnIeškoti;
        private System.Windows.Forms.TextBox txtPavardė;
        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pavardė;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dalykas;
        private System.Windows.Forms.DataGridViewTextBoxColumn DalykoId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxDalykai;
    }
}